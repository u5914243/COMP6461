import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by willmao on 9/8/17.
 */
public class MyCav1 extends JComponent {

    public MyCav1() {
        this.setPreferredSize(new Dimension(400,400));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        BufferedImage buff = new BufferedImage(400,400,BufferedImage.TYPE_INT_ARGB);
        g.setColor(Color.black);
        int x0 = 100;
        int xn = 200;
        int y0 = 100;
        int yn = 300;
        double xk = x0;
        double yk = y0;
        for(double i=0.0;i<1.0;i+=0.001){
            xk = x0 + i*(xn-x0);
            yk = y0 + i*(yn-y0);
            buff.setRGB((int) xk,(int) yk,Color.RED.getRGB());
            g.drawImage(buff,0,0,null);
        }
        g.drawLine(100,100,200,300);
    }
}
