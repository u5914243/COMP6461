import javax.swing.*;

/**
 * Created by willmao on 8/8/17.
 */
public class MyDrawing implements Runnable {
    JFrame jframe;
    MyCav1 mycav;
    public MyDrawing(){
        SwingUtilities.invokeLater(this);

    }
    public static void main(String[] args){
        new MyDrawing();

    }

    @Override
    public void run() {
        jframe = new JFrame("My Drawing!");
        mycav = new MyCav1();
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.getContentPane().add(mycav);
        jframe.pack();
        jframe.setVisible(true);
    }
}
