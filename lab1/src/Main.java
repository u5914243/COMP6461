import javax.swing.*;

public class Main implements Runnable{
    JFrame jframe;
    MyCanvas canvas;

    public Main(){
        SwingUtilities.invokeLater( this);
    }

    public static void main(String[] args) {
        new Main();
        System.out.println(1.0f/2.0f);

    }

    @Override
    public void run() {
        jframe = new JFrame("GUI Prgram 2");
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        canvas = new MyCanvas();
        jframe.getContentPane().add(canvas);
        jframe.pack();
        jframe.setVisible(true);

    }
}
