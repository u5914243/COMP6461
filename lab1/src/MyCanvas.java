import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by willmao on 8/8/17.
 */
public class MyCanvas extends JComponent{
    BufferedImage buff;
    public MyCanvas(){
        this.setPreferredSize(new Dimension(300,300));
        buff = new BufferedImage(300,300,BufferedImage.TYPE_INT_ARGB);

        Graphics g = buff.getGraphics();
        g.setColor(Color.white);
        g.fillRect(0,0,300,300);

        buff.setRGB(100,100,Color.red.getRGB());

        int x0 = 100;
        int y0 = 100;
        int xn = 100;
        int yn = 100;
        /*
        for(double t=0.0;t<=1.0;t+=0.0001){
            double px = x0 + t*(xn-x0);
            double py = x0 + t*(yn-y0);
            buff.setRGB((int)px,(int)py,Color.black.getRGB());
        }
        */
        /*
        for(int t=x0;t<=xn;t++){
            int y = 50*t/200 + 50;
            buff.setRGB(t,y,Color.black.getRGB());
        }
        */


        /*
        Color.RED.getRGB();

        int dx = Math.abs(xn-x0);
        int dy = Math.abs(yn-y0);
        int xk;
        int yk;
        if(x0<xn){
            xk=x0;
            yk=y0;
        }else {
            xk = xn;
            yk = yn;
        }
        buff.setRGB(xk,yk,Color.BLACK.getRGB());
        if(dy <= dx){
            int p = 2*dy - dx;
            for (int i = 0; i < dx; i++){
                xk++;
                if(p < 0){
                    p = p + 2*dy;
                }else{
                    p = p + 2*dy - 2*dx;
                    if(dy*dx==(yn-y0)*(xn-x0)){
                        yk++;
                    }
                    else{
                        yk--;
                    }
                }
                buff.setRGB(xk,yk,Color.black.getRGB());
            }
        }else{
            int p = 2*dx - dy;
            for (int i = 0; i < dy; i++){
                yk++;
                if(p < 0){
                    p = p + 2*dx;
                }else{
                    p = p + 2*dx - 2*dy;
                    if(dy*dx==(yn-y0)*(xn-x0)){
                        xk++;
                    }
                    else{
                        xk--;
                    }
                }
                buff.setRGB(xk,yk,Color.black.getRGB());
            }
        }*/


        /*
        int dx = Math.abs(xn-x0);
        int dy = Math.abs(yn-y0);
        int xk;
        int yf;
        int yc;
        float yr;

        int yk;
        int xf;
        int xc;
        float xr;

        float df;
        float dc;
        buf.setRGB(x0,y0,rgb);
        if(dx>=dy){
            if (xn!=x0){
                float m = (float)(yn-y0)/(float)(xn-x0);
                float b = (float)y0-m*x0;
                xk=x0;
                for(int i = 0;i < dx; i++){
                    if(x0>xn){
                        xk--;
                    }else if(x0<xn){
                        xk++;
                    }
                    yr = m*(float)xk + b;
                    yf = (int)Math.floor(yr);
                    yc = (int)Math.ceil(yr);
                    df = Math.abs(yf-yr);
                    dc = Math.abs(yc-yr);
                    if(df>dc){
                        buf.setRGB(xk,yc,rgb);
                    }else{
                        buf.setRGB(xk,yf,rgb);
                    }
                }
            }
        }else{
            if (yn!=y0){
                float m = (float)(xn-x0)/(float)(yn-y0);
                float b = (float)x0-m*y0;
                yk=y0;
                for(int i = 0;i < dy; i++){
                    if(y0>yn){
                        yk--;
                    }else if(y0<yn){
                        yk++;
                    }
                    xr = m*(float)yk + b;
                    xf = (int)Math.floor(xr);
                    xc = (int)Math.ceil(xr);
                    df = Math.abs(xf-xr);
                    dc = Math.abs(xc-xr);
                    if(df>dc){
                        buf.setRGB(xc,yk,rgb);
                    }else{
                        buf.setRGB(xf,yk,rgb);
                    }
                }
            }
        }*/





        this.invalidate();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(buff,0,0,null);

        /*
        g.setColor(Color.white);
        g.fillRect(0,0,300,300);

        g.setColor(new Color(1.0f,0.0f,0.0f));
        g.fillRect(20,30,100,30);

        //g.drawLine(10,50,15,250);

        g.setColor(new Color(0.0f,1.0f,0.0f));
        g.drawRoundRect(40,30,100,50,15,15);

        g.drawOval(200,200,40,40);

        g.setColor(new Color(0.1f,0.1f,0.1f));
        String str="Hello";

        g.setFont(new Font(Font.MONOSPACED,Font.BOLD,30));
        int w = g.getFontMetrics().stringWidth(str);
        int sx = (300 - w)/2;
        g.drawString(str,sx,150);
        g.drawRect(sx,120,w,30);

        BufferedImage image;

        try {
            image = ImageIO.read(new File("img/compgraphicslogo.png"));
            Graphics g2 = image.getGraphics();
            g2.setColor(new Color(0.0f,1.0f,0.0f));
            g2.drawLine(0,0,100,400);
            g.drawImage(image,150,200,null);

        } catch (IOException e) {
            e.printStackTrace();
        }

        Graphics2D g3 = (Graphics2D) g;
        g3.draw(new Line2D.Double(10.0,50.0,15.0,250.0));
        Path2D p = new Path2D.Double();
        p.moveTo(200,10);
        p.quadTo(100,100,210,150);
        p.quadTo(100,300,200,110);
        g3.fill(p);
        g3.setColor(Color.red);
        g3.draw(p);
        */

    }
}
